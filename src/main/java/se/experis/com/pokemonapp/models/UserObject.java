package se.experis.com.pokemonapp.models;

import se.experis.com.pokemonapp.models.PokemonObject;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class UserObject {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    @Column(unique = true)
    public String userName;

    @Column
    public String password;

    @OneToMany(targetEntity=PokemonObject.class,cascade = CascadeType.ALL,
            fetch = FetchType.LAZY, orphanRemoval = true )
    public Set<PokemonObject> pokemons = new HashSet<>();

}
