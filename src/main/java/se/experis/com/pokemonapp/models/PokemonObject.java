package se.experis.com.pokemonapp.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.Date;


@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class PokemonObject {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    // Used to distinguish corresponding  information from the API
    @Column
    public Integer apiId;

    @Column
    public String name;

    // Could be M(ale) ,F(emale) or G(enderless)
    @Column
    public Character gender;

    @Column
    public Boolean shiny;

    // Based on the calculations for rares Pokemons
    @Column
    public Boolean rare;

    // A date generated in the instantiation of the pokemonObject
    @Column
    public Date date;

    public PokemonObject() {
        Date date = new Date();
        this.date = date;
    }

    @Column
    public String location;

    @ManyToOne(fetch = FetchType.LAZY)
    private PokemonObject pokemonObject;
}
