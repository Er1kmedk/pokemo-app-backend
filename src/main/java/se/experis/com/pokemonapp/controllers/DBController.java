package se.experis.com.pokemonapp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.experis.com.pokemonapp.models.PokemonObject;
import se.experis.com.pokemonapp.models.UserObject;
import se.experis.com.pokemonapp.repositories.PokemonRepository;
import se.experis.com.pokemonapp.repositories.UserRepository;
import se.experis.com.pokemonapp.utils.Command;
import se.experis.com.pokemonapp.utils.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;

@RestController
public class DBController {

    HashMap<String, Integer> pokemonMap = new HashMap<String, Integer>();
    ArrayList occurrence = new ArrayList<String>();


    @Autowired
    private PokemonRepository pokemonRepository;

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/pokemonapp/pokemon/getRarest")
    public ResponseEntity<CommonResponse> getRarest(HttpServletRequest request) {
        CommonResponse cr = new CommonResponse();

        for (Object apiId: occurrence) {
            String apiIdKey = apiId.toString();
            if (pokemonMap.containsKey(apiIdKey)) {
                pokemonMap.put(apiIdKey, pokemonMap.get(apiIdKey) + 1);
            }
            else {
                System.out.println("Added apiId: " + apiId);
                pokemonMap.put(apiIdKey, 1);
            }
        }

        HttpStatus resp = HttpStatus.OK;
        cr.data = pokemonMap;
        cr.message = "Rarest Pokemons";


        return new ResponseEntity<>(cr, resp);
    }


    @PostMapping("/pokemonapp/pokemon/new")
    public PokemonObject addPokemon(HttpServletRequest request, HttpServletResponse response, @RequestBody PokemonObject pokemonObject) {
        occurrence.add(pokemonObject.apiId);
        return pokemonRepository.save(pokemonObject);
    }

    // Add pokemon to specific user
    @PostMapping("/pokemonapp/add-pokemon/{userName}")
    public ResponseEntity<CommonResponse> addPokemonToUser(@RequestBody PokemonObject pokemonObject, @PathVariable String userName) {
        CommonResponse cr = new CommonResponse();
        HttpStatus resp;

        if(userRepository.existsByUserName(userName)) {
            UserObject user = userRepository.findByUserName(userName);
            user.pokemons.add(pokemonObject);
            occurrence.add(pokemonObject.apiId);
            user = userRepository.save(user);
            cr.data = user;
            cr.message = "New pokemon created!";
            resp = HttpStatus.CREATED;
        } else {
            cr.data = null;
            cr.message = "No user has username " + userName;
            resp = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(cr, resp);
    }

    // Get all the pokemons for a specific user
    @GetMapping("/pokemonapp/get-all-pokemons/{userName}")
    public ResponseEntity<CommonResponse> getUserSecificPokemons(@PathVariable String userName) {
        CommonResponse cr = new CommonResponse();
        HttpStatus resp;
        if(userRepository.existsByUserName(userName)) {
            UserObject user = userRepository.findByUserName(userName);
            cr.data = user.pokemons;
            cr.message = "All pokemons for user " + userName;
            resp = HttpStatus.CREATED;
        } else {
            cr.data = null;
            cr.message = "No user has username " + userName;
            resp = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(cr, resp);
    }


    @GetMapping("/pokemonapp/pokemon/all")
    public ResponseEntity<CommonResponse> getAllPokemons(HttpServletRequest request){
        Command cmd = new Command(request);

        // Process
        CommonResponse cr = new CommonResponse();
        cr.data = pokemonRepository.findAll();
        cr.message = "All pokemons";

        HttpStatus resp = HttpStatus.OK;

        // Log and return answer
        cmd.setResult(resp);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(cr, resp);
    }

    @PostMapping("/pokemonapp/user/new")
    public UserObject addUser(HttpServletRequest request, HttpServletResponse response, @RequestBody UserObject userObject) {
        return userRepository.save(userObject);
    }

    // Get a specific user
    @GetMapping("/pokemonapp/user/{userName}")
    public ResponseEntity<CommonResponse> getSpecificUser(HttpServletRequest request, @PathVariable String userName) {
        Command cmd = new Command(request);
        CommonResponse cr = new CommonResponse();
        HttpStatus resp;
        if (!userRepository.existsByUserName(userName)) {
            cr.data = null;
            cr.message = "User not found";
            resp = HttpStatus.NOT_FOUND;
        } else {
            cr.data = userRepository.findByUserName(userName);
            resp = HttpStatus.OK;
            cr. message = "User with userName: " + userName;
        }
        //log and return
        cmd.setResult(resp);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(cr, resp);
    }


    @GetMapping("/pokemonapp/user/all")
    public ResponseEntity<CommonResponse> getAllUsers(HttpServletRequest request){
        Command cmd = new Command(request);

        // Process
        CommonResponse cr = new CommonResponse();
        cr.data = userRepository.findAll();
        cr.message = "All users";

        HttpStatus resp = HttpStatus.OK;

        // Log and return answer
        cmd.setResult(resp);
        Logger.getInstance().logCommand(cmd);
        return new ResponseEntity<>(cr, resp);
    }

}

class CommonResponse {
    public Object data;
    public String message;
}
