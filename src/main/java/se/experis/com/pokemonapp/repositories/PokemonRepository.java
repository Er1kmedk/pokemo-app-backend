package se.experis.com.pokemonapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import se.experis.com.pokemonapp.models.PokemonObject;

import java.util.List;

public interface PokemonRepository extends JpaRepository<PokemonObject, Integer> {
    PokemonObject getById(Integer id);
}
