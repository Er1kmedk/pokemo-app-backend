package se.experis.com.pokemonapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import se.experis.com.pokemonapp.models.UserObject;

public interface UserRepository extends JpaRepository<UserObject, Integer> {
    boolean existsByUserName(String userName);
    UserObject findByUserName(String userName);
}
