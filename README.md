Erik Johansson, Martin Sandström, Angelina Fransson

// Create a new Pokemon
@PostMapping("/pokemonapp/pokemon/new")

// Add a Pokemon to a specific (existing) user
@PostMapping("/pokemonapp/add-pokemon/{userObjectId}")

// Return a list of all Pokemons in the Db
@GetMapping("/pokemonapp/pokemon/all")

// Create a new user
 @PostMapping("/pokemonapp/user/new")

// Get a specific user
@GetMapping("/pokemonapp/user/{userName}")

// Get all users
@GetMapping("/pokemonapp/user/all")

// Get all the pokemons for a specific user
@GetMapping("/pokemonapp/get-all-pokemons/{userName}")